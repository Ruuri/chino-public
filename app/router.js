import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('track', {path: '/tracks/:track_id/:title'}, function(){
    
  });
  this.route('album', {path: '/albums/:album_id'}, function(){
    
  });
  this.route('artist', {path: '/artists/:artist_id'}, function(){
    
  });
  this.route('likes', {path: '/likes'}, function(){
    
  });
  this.route('post', {path: '/posts/:album_id'}, function(){
    
  });
  this.route('posts');
  this.route('playlist', {path: '/playlists/:playlist_id'}, function(){

  });
  this.route('playlists');
  this.route('search', {path: '/search'}, function(){

  });
  this.route('lastListenedTo', {path: '/listening'});
});

export default Router;
