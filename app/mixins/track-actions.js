import Ember from 'ember';

export default Ember.Mixin.create({
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",
  likeTrack: "likeTrack",
  unlikeTrack: "unlikeTrack",
  
  actions: {
    playTrack(track, position){
      this.sendAction("playTrack", track, position, this.get("tracks"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    }
  }
});
