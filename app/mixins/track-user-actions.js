import Ember from 'ember';
import config from '../config/environment';
const { service } = Ember.inject;

export default Ember.Mixin.create({
  session: Ember.inject.service("session"),
  currentUser: Ember.inject.service("current-user"),
  store: Ember.inject.service(),
  
  listenToTrack(){
    const endpoint = config.host + "/tracks/" + this.get("track.id") + "/listen/";
    $.post(endpoint);
  },

  actions: {
    likeTrack(){
      const endpoint = config.host + "/tracks/" + this.get("track.id") + "/like";
      let store = this.get("store");
      let self = this;
      
      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            // stuff
          })
        }).done(function(response){
          store.pushPayload("track", response);
        }).fail(function(){
          self.get("track").reload();
        });
      });
    },
    unlikeTrack(){
      const endpoint = config.host + "/tracks/" + this.get("track.id") + "/unlike";
      let store = this.get("store");
      let self = this;

      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            
          })
        }).done(function(response){
          store.pushPayload("track", response);
        }).fail(function(){
          self.get("track").reload();
        });
      });
    },
    addToPlaylist(playlist, track){
      const endpoint = config.host + "/playlists/" + playlist.get("id") + "/add_track/";
      let store = this.get("store");
      let self = this;

      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            track: track.get("id")
          })
        }).done(function(response){
          store.pushPayload("playlist", response);
        }).fail(function(){
          self.get("track").reload();
        });
      });
    }
  }
});
