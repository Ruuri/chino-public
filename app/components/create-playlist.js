import Ember from 'ember';
import config from '../config/environment';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  currentUser: Ember.inject.service("current-user"),
  session: Ember.inject.service("session"),
  name: "",
  addedTrackToPlaylist: "addedTrackToPlaylist",
  classNames: ["create-playlist-wrapper"],

  canCreatePlaylist: Ember.computed("name.length", function(){
    return this.get("name.length") > 0;
  }),

  actions: {
    createPlaylist(){
      if(this.get("canCreatePlaylist")){
        let store = this.get("store");
        let name = this.get("name");
        const trackId = this.get("track.id");
        let self = this;

        this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
          let playlist = store.createRecord('playlist', {
            name: name,
            trackholder: trackId
          });
          playlist.save().then(function(){
            self.sendAction("addedTrackToPlaylist");
          });
        });
      }
    }
  }

});
