import Ember from 'ember';
import TrackUserActions from '../mixins/track-user-actions';

export default Ember.Component.extend(TrackUserActions, {
  tagName: "tr",
  classNameBindings: ["track.playing:playing"],
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",

  actions: {
    playTrack(){
      this.sendAction("playTrack", this.get("track"), this.get("track.order"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    }
  }
});
