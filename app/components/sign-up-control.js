import Ember from 'ember';
import config from '../config/environment';

const { service } = Ember.inject;

export default Ember.Component.extend({
  classNames: ["signUp__container"],
  email: "",
  password: "",
  store: Ember.inject.service(),
  needsPassword: true,
  needsEmail: true,
  signingIn: false,
  waiting: false,
  emailTaken: false,
  session: service('session'),
  currentUser: service('current-user'),

  validateInput(){
    if(this.get("email.length") === 0){
      this.set("needsEmail", true);
    }else{
      this.set("needsEmail", false);
    }
    if(this.get("password.length") === 0){
      this.set("needsPassword", true);
    }else{
      this.set("needsPassword", false);
    }

    return !(this.get("needsEmail") && this.get("needsPassword"));
  },
  canSignIn: Ember.computed('needsEmail', 'needsPassword', function(){
    return !( this.get("needsPassword") && this.get("needsPassword") );
  }),
  actions: {
    signUp(){
      if(this.validateInput()){
        let self = this;
        let user = {
          user: {
            email: this.get("email"),
            password: this.get("password")  
          } 
        }
        const endpoint = config.host + "/users";
        this.set("waiting", true);

        $.post(endpoint, user)
        .done(function(response){
          self.send("authenticate");
        })
        .fail(function(){
          self.set("emailTaken", true);
          self.set("waiting", false);
        });
      }
    },
    authenticate(){
      if(this.validateInput()){
        let { email, password } = this.getProperties('email', 'password');
        let currentUser = this.get("currentUser");

        this.set("waiting", true);
        return this.get('session').authenticate('authenticator:devise', email, password);
      }
    },
    validate(){
      this.validateInput();
    },
    toggleSignIn(){
      this.toggleProperty("signingIn");
    }
  }
});
