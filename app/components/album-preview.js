import Ember from 'ember';
import config from '../config/environment';

export default Ember.Component.extend({
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",
  session: Ember.inject.service("session"),

  albumDownloadLink: Ember.computed("album.id", function(){
    return config.host + "/albums/" + this.get("album.id") + "/download";
  }),

  actions: {
    playTrack(track, position){
      this.sendAction("playTrack", track, position, this.get("album.orderedTracks"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    },
  }
});
