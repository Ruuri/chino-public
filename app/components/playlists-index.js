import Ember from 'ember';

export default Ember.Component.extend({
  playlists: null,
  store: Ember.inject.service(),

  didReceiveAttrs(){
    this.set("playlists", this.get("store").findAll("playlist"));
  }
});
