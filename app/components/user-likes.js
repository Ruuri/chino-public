import Ember from 'ember';
import config from '../config/environment';
import TrackActions from '../mixins/track-actions';

export default Ember.Component.extend(TrackActions,{
  tracks: Em.A(),
  store: Ember.inject.service(),
  session: Ember.inject.service("session"),
  currentUser: Ember.inject.service("current-user"),
  loading: false,

  didReceiveAttrs(){
    this.set("tracks", Em.A());
    this.loadTracks();
    this.bindScrolling();
  },
  willDestroyElement(){
    this.unbindScrolling();
  },
  bindScrolling(opts) {
    var onScroll, _this = this;
    onScroll = function(){ 
      return _this.scrolled();
    }

    $(document).bind('touchmove', onScroll);
    $(window).bind('scroll', onScroll);
  },

  unbindScrolling() {
    $(window).unbind('scroll');
    $(document).unbind('touchmove');
  },

  scrolled(){
    Ember.run.debounce(this, this.handleScroll, 200)
  },
  handleScroll(){
    if(!this.get("reloading")){
      if($(window).scrollTop() < 1000 && $(document).height() < 10000){
        this.loadMoreHelper()
      }else if($(window).scrollTop() + $(window).height() >= $(document).height() - 500){
        this.loadMoreHelper()
      }
    }
  },
  loadTracks(){
    if(!this.get("reloading")){
      this.set("reloading", true);
      const endpoint = config.host + "/users/" + this.get("user.id") + "/likes";
      let store = this.get("store");
      let tracks = this.get("tracks");
      let self = this;

      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'GET',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            // stuff
          })
        }).done(function(response){
          store.pushPayload("track", response);
           for(let track of response.data){
            tracks.pushObject(store.peekRecord("track", track.id));
          }
          self.set("reloading", false);
        });
      });
    }
  },
  loadMoreHelper(){
    if(!this.get("reloading")){
      this.set("reloading", true);
      const endpoint = config.host + "/users/" + this.get("user.id") + "/likes?offset=" + this.get("tracks.length");
      let store = this.get("store");
      let tracks = this.get("tracks");
      let self = this;

      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'GET',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            // stuff
          })
        }).done(function(response){
          store.pushPayload("track", response);
           for(let track of response.data){
            tracks.pushObject(store.peekRecord("track", track.id));
          }
          self.set("reloading", false);
        });
      });
    }
  }
});
