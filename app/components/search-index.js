import Ember from 'ember';
import config from '../config/environment';
import TrackActions from '../mixins/track-actions';

export default Ember.Component.extend(TrackActions, {
  tracks: Em.A(),
  store: Ember.inject.service(),
  session: Ember.inject.service("session"),

  didReceiveAttrs(){
    this.set("tracks", Em.A());
    this.searchQuery();
  },

  searchQuery(){
    const endpoint = config.host + "/search?query=" + this.get("query");
    let store = this.get("store");
    let tracks = this.get("tracks");

    if(this.get("session.isAuthenticated")){
      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'GET',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            
          })
        }).done(function(response){
          store.pushPayload("track", response);
          response.data.forEach(function(track){
            tracks.pushObject(store.peekRecord('track', track.id));
          });
        }).fail(function(){
          
        });
      });
    }else{
      $.getJSON(endpoint)
      .done(function(response){
        store.pushPayload("track", response);

        response.data.forEach(function(track){
          tracks.pushObject(store.peekRecord('track', track.id));
        });
      });
    }
  }

});
