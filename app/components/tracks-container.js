import Ember from 'ember';

export default Ember.Component.extend({
  tracks: Ember.A(),
  store: Ember.inject.service(),
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",
  loading: false,

  didReceiveAttrs(){
    this.loadMoreHelper();
    this.bindScrolling();
  },
  willDestroyElement(){
    this.unbindScrolling();
  },
  bindScrolling(opts) {
    var onScroll, _this = this;
    onScroll = function(){ 
      return _this.scrolled();
    }

    $(document).bind('touchmove', onScroll);
    $(window).bind('scroll', onScroll);
  },

  unbindScrolling() {
    $(window).unbind('scroll');
    $(document).unbind('touchmove');
  },

  scrolled(){
    Ember.run.debounce(this, this.handleScroll, 200)
  },

  handleScroll(){
    if(!this.get("reloading")){
      if($(window).scrollTop() < 1000 && $(document).height() < 10000){
        this.loadMoreHelper()
      }else if($(window).scrollTop() + $(window).height() >= $(document).height() - 1000){
        this.loadMoreHelper()
      }
    }
  },
  loadMoreHelper(){
    if(!this.get("loading")){
      this.set("loading", true);
      
      const offset = this.get("tracks.length");
      const store = this.get("store");
      let tracks = this.get("tracks");
      let self = this;

      store.query("track", {offset: offset}).then(function(loaded){
        loaded.forEach(function(track, i){
          tracks.pushObject(track);
        });
        self.set("loading", false);
      });
    }
  },

  actions: {
    playTrack(track, position){
      this.sendAction("playTrack", track, track.get("album.orderedTracks").indexOf(track), track.get("album.orderedTracks"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    }
  }
});
