import Ember from 'ember';
import TrackUserActions from '../mixins/track-user-actions';

export default Ember.Component.extend(TrackUserActions, {
  classNames: ["player__sidebar"],
  classNameBindings: ["track:playing"],

  lastPlayed: {},
  playing: false,
  player: null,
  currentTime: 0,
  volume: 1,
  muted: false,
  addingToPlaylist: false,
  repeat: false,

  didInsertElement(){
    const audio = document.getElementById('hibikiplayer');
    audio.addEventListener('loadeddata', this.setCanPlay.bind(this));
    audio.addEventListener('ended', this.finishedTrack.bind(this));
    audio.addEventListener('timeupdate', this.updateCurrentTime.bind(this));
    audio.addEventListener('volumechange', this.updateVolume.bind(this));
    this.set("volume", audio.volume);
  },
  updateCurrentTime(){
    this.set("currentTime", document.getElementById('hibikiplayer').currentTime);
  },
  updateVolume(){
    this.set("volume", document.getElementById('hibikiplayer').volume);
  },
  twitterURL: Ember.computed('track.id', 'track.title', 'track.album.title', function(){
    const url = encodeURI("https://animemusic.moe/tracks/" + this.get("track.id") + "/" + encodeURIComponent(this.get("track.title")));
    const text = encodeURIComponent("#nowplaying " + this.get("track.title") + " | " + this.get("track.album.title") + " #anime");
    return "https://twitter.com/intent/tweet?url=" + url + "&text=" + text; 
  }),
  currentTimeFormatted: Ember.computed("currentTime", function(){

    const minutes = Math.floor(this.get("currentTime")/60);
    let seconds = Math.floor(this.get("currentTime") - (60*minutes));

    if(seconds < 10){
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  }),
  pauseTrackObserver: Ember.observer('pauseTrack', function(){
    this.send("pause");
  }),
  playMusicObserver: Ember.observer('playMusic', function(){
    if(this.get("lastPlayed.id") === this.get("track.id")){
      const player = document.getElementById('hibikiplayer');
      player.play();
      this.set("track.playing", true);
      this.set("playing", true);
    }
  }),
  durationPlayerStyle: Ember.computed('currentTime', 'track.length', function(){
    const endTime = this.get("track.length");
    const currentTime = this.get("currentTime");

    const width = (currentTime/endTime)*100;
    const style = "width: " + width + "%;";
    return Ember.String.htmlSafe(style);
  }),
  volumeBarStyle: Ember.computed('volume', 'muted', function(){
    let style = "";
    if(this.get("muted")){
      style = "width: 0";
    }else{
      let width = 0;
      if(this.get("volume") > 0.9){
        width = 200;
      }else{
        width = 200*this.get("volume");  
      }
      style = "width: " + width + "px;"
    }

    return Ember.String.htmlSafe(style);
  }),
  setCanPlay(){
    this.set("canPlay", true);
    this.send("playTrack");
  },
  finishedTrack(){
    if(this.get("repeat")){
      const player = document.getElementById('hibikiplayer');
      player.currentTime = 0;
      player.play();
    }else{
      this.send("nextTrack");
    }
  },

  actions: {
    seek(event){
      const position = event.clientX - $(".player-duration").offset().left;
      const seekTime = (position * this.get("track.length"))/210; //210 being the width of the seek bar
      document.getElementById('hibikiplayer').currentTime = seekTime;
    },
    setVolume(event){
      if(this.get("muted")){
        this.set("muted", false);
        document.getElementById('hibikiplayer').muted = false;
      }
      const position = event.clientX - ($(".player-volume-bar-container").offset().left);
      const volume = position/200;

      document.getElementById('hibikiplayer').volume = volume;
    },
    toggleMute(){
      if(this.get("muted")){
        this.set("muted", false);
        document.getElementById('hibikiplayer').muted = false;
      }else{
        this.set("muted", true);
        document.getElementById('hibikiplayer').muted = true;
      }
    },
    pause(){
      this.set("playing", false);
      this.set("loading", false);
      this.set("track.playing", false);
      this.set("track.loading", false);
      const player = document.getElementById('hibikiplayer');
      player.pause();
    },
    playTrack(){
      if(this.get("lastPlayed.id") != this.get("track.id")){
        this.set("lastPlayed.playing", false);
        this.set("lastPlayed.loading", false);
        this.listenToTrack();
      }
      const player = document.getElementById('hibikiplayer');
      player.play();
      this.set("track.playing", true);
      this.set("track.loading", false);
      this.set("playing", true);
      this.set("lastPlayed", this.get("track"));
      document.title = this.get("track.title") + " | animemusic";
    },
    nextTrack(){
      const player = document.getElementById('hibikiplayer');
      player.pause();
      this.incrementProperty("position");

      if(this.get("trackList.length") > this.get("position")){
        this.set("track.playing", false);
        this.set("track", null);
        this.set("track", this.get("trackList").objectAt(this.get("position")));
        this.set("track.loading", true);
        document.title = this.get("track.title") + " | animemusic";
      }else{
        this.decrementProperty("position");
        this.send("pause");
      }
    },
    previousTrack(){
      const player = document.getElementById('hibikiplayer');
      player.pause();
      this.decrementProperty("position");

      if(this.get("position") >= 0){
        this.set("track.playing", false);
        this.set("track", this.get("trackList").objectAt(this.get("position")));
        this.set("track.loading", true);
        document.title = this.get("track.title") + " | animemusic";
      }else{
        this.incrementProperty("position");
        this.send("pause");
      }
    },
    queueTrack(track, position){
      this.set("track", track);
      this.set("position", position);
    },
    toggleAddToPlaylist(){
      this.toggleProperty("addingToPlaylist");
    },
    addedTrackToPlaylist(){
      this.set("addingToPlaylist", false);
    },
    toggleRepeat(){
      this.toggleProperty("repeat");
    }
  }
});
