import Ember from 'ember';
import TrackUserActions from '../mixins/track-user-actions';
Ember.TextField.reopen({
  attributeBindings: ['data-clipboard-text']
});
export default Ember.Component.extend(TrackUserActions,{
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",

  trackPermalink: Ember.computed('track.id', 'track.title', function(){
    return "https://animemusic.moe/tracks/" + this.get("track.id") + "/" + this.get("track.title");
  }),
  twitterURL: Ember.computed('track.id', 'track.title', 'track.album.title', function(){
    const url = encodeURI("https://animemusic.moe/tracks/" + this.get("track.id") + "/" + encodeURIComponent(this.get("track.title")));
    const text = encodeURIComponent("#nowplaying " + this.get("track.title") + " | " + this.get("track.album.title") + " #anime");
    return "https://twitter.com/intent/tweet?url=" + url + "&text=" + text; 
  }),

  didInsertElement(){
    new Clipboard('.track-index-share-button');
  },


  actions: {
    playTrack(track){
      this.set("track.loading", true);
      this.sendAction("playTrack", track, track.get("order")-1, track.get("album.tracks"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack")
    },
    playCurrentTrack(){
      this.send("playTrack", this.get("track"));
    }
  }
});
