import Ember from 'ember';
import config from '../config/environment';
import TrackActions from '../mixins/track-actions';

export default Ember.Component.extend(TrackActions,{
  tracks: Em.A(),
  store: Ember.inject.service(),
  session: Ember.inject.service("session"),
  tagName: "article",
  loading: false,
  reloading: false,

  didReceiveAttrs(){
    this.set("tracks", Em.A());
    this.loadMoreHelper();
    this.bindScrolling();
  },
  willDestroyElement(){
    this.unbindScrolling();
  },
  bindScrolling(opts) {
    var onScroll, _this = this;
    onScroll = function(){ 
      return _this.scrolled();
    }

    $(document).bind('touchmove', onScroll);
    $(window).bind('scroll', onScroll);
  },
  unbindScrolling() {
    $(window).unbind('scroll');
    $(document).unbind('touchmove');
  },

  scrolled(){
    Ember.run.debounce(this, this.handleScroll, 200)
  },

  handleScroll(){
    if(!this.get("reloading")){
      if($(window).scrollTop() < 1000 && $(document).height() < 10000){
        this.loadMoreHelper();
      }else if($(window).scrollTop() + $(window).height() >= $(document).height() - 1000){
        this.loadMoreHelper();
      }
    }
  },

  loadMoreHelper(){
    if(!this.get("loading")){
      this.set("loading", true);
      const endpoint = config.host + "/tracks/last_listened_to?limit=24&offset=" + this.get("tracks.length");
      let tracks = this.get("tracks");
      let store = this.get("store");
      let self = this;

      if(this.get("session.isAuthenticated")){
        this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
          Ember.$.ajax({
            url: endpoint,
            beforeSend: function(xhr) {
              xhr.setRequestHeader(headerName, headerValue);
            },
            method: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
              
            })
          }).done(function(response){
            store.pushPayload("track", response);
            response.meta.played.forEach(function(id){
              tracks.pushObject(store.peekRecord('track', id));
            });
            self.set("loading", false);
          }).fail(function(){
            self.set("loading", false);
          });
        });
      }else{
        $.getJSON(endpoint)
        .done(function(response){
          store.pushPayload("track", response);
          response.data.forEach(function(track){
            tracks.pushObject(store.peekRecord('track', track.id));
          });
          self.set("loading", false);
        });
      }
    }
  }
});
