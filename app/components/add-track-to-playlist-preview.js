import Ember from 'ember';
import TrackUserActions from '../mixins/track-user-actions';

export default Ember.Component.extend(TrackUserActions, {
  addedTrackToPlaylist: "addedTrackToPlaylist",

  actions: {
    addTrackToPlaylist(){
      this.send("addToPlaylist", this.get("playlist"), this.get("track"));
      this.sendAction("addedTrackToPlaylist");
    }
  }  
});
