import Ember from 'ember';

export default Ember.Component.extend({
  title: "",
  description: "",
  store: Ember.inject.service(),
  routing: Ember.inject.service('-routing'),

  actions: {
    createPost(){
      let post = this.get("store").createRecord('post', {
        title: this.get("title"),
        description: this.get("description"),
      });
      let routing = this.get("routing");
      post.save().then(function(post){
        routing.transitionTo("post.index", post.get("id"));
      });
    }
  }
});
