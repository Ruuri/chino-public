import Ember from 'ember';
import TrackActions from '../mixins/track-actions';

export default Ember.Component.extend(TrackActions, {
  tracks: Ember.computed("playlist.tracks", function(){
    return this.get("playlist.tracks");
  })
});
