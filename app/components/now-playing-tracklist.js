import Ember from 'ember';
import { PerfectScrollbarMixin } from 'ember-perfect-scrollbar';

export default Ember.Component.extend(PerfectScrollbarMixin, {
  classNames: ["player-playlist"],
  queueTrack: "queueTrack",

  actions: {
    queueTrack(track, position){
      this.sendAction("queueTrack", track, position);
    }
  }
});
