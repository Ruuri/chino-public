import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ["nav-search"],
  router:  Ember.inject.service('-routing'),
  query: null,

  actions: {
    search(){
      this.get("router").transitionTo('search.index',[], {query: this.get("query")});
    }
  }
});
