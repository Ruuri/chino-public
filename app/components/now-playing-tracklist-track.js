import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ["now-playing-tracklist-track"],
  classNameBindings: ["nowPlaying:playing"],
  queueTrack: "queueTrack",

  nowPlaying: Ember.computed('position', 'index', function(){
    if(this.get("position") === this.get("index")){
      if($(".player__sidebar")[0].offsetWidth === 0){
        Ember.run.next(this, function(){
          this.$()[0].scrollIntoView();
        });
      }else{
        this.$()[0].scrollIntoView();
      }
      return true;
    }else{
      return false;
    }
  }),

  click(e){
    this.send("queueTrack");
  },

  actions: {
    queueTrack(){
      this.sendAction("queueTrack", this.get("track"), this.get("index"));
    }
  }
});
