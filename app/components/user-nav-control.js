import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ["nav__control"],
  session: Ember.inject.service("session"),
  currentUser: Ember.inject.service("current-user"),
  viewingDropdown: false,

  actions: {
    toggleDropdown(){
      this.toggleProperty("viewingDropdown");
    },
    logout(){
      this.get("session").invalidate();
    }
  }
});
