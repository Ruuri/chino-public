import Ember from 'ember';
const { service } = Ember.inject;

export default Ember.Component.extend({
  classNames: ["add-track-to-playlist-wrapper"],
  playlists: null,
  store: Ember.inject.service(),
  addedTrackToPlaylist: "addedTrackToPlaylist",

  didReceiveAttrs(){
    this.loadPlaylists();
  },
  loadPlaylists(){
    this.set("playlists", this.get("store").findAll('playlist'));
  },

  actions: {
    addedTrackToPlaylist(){
      this.sendAction("addedTrackToPlaylist");
    }
  }
});
