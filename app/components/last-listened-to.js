import Ember from 'ember';
import config from '../config/environment';
import TrackActions from '../mixins/track-actions';

export default Ember.Component.extend(TrackActions,{
  classNames: ["last-listened-to-container"],
  tracks: Em.A(),
  store: Ember.inject.service(),
  session: Ember.inject.service("session"),
  tagName: "article",


  didReceiveAttrs(){
    this.set("tracks", Em.A());
    this.loadTracks();
  },

  loadTracks(){
    const endpoint = config.host + "/tracks/last_listened_to";
    let tracks = this.get("tracks");
    let store = this.get("store");

    if(this.get("session.isAuthenticated")){
      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'GET',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            
          })
        }).done(function(response){
          store.pushPayload("track", response);
          response.meta.played.forEach(function(id){
            tracks.pushObject(store.peekRecord('track', id));
          });
        }).fail(function(){
          
        });
      });
    }else{
      $.getJSON(endpoint)
      .done(function(response){
        store.pushPayload("track", response);
        response.data.forEach(function(track){
          tracks.pushObject(store.peekRecord('track', track.id));
        });
      });
    }
  }
});
