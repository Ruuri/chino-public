import Ember from 'ember';

export default Ember.Component.extend({
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",
  
  actions: {
    playTrack(track){
      this.sendAction("playTrack", track, track.get("order"), track.get("album"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    }
  }
});
