import Ember from 'ember';

export default Ember.Component.extend({
  classNameBindings: ["track::hidden"],
  lastPlayed: {},
  playing: false,
  player: null,
  currentTime: 0,
  volume: 1,
  muted: false,

  didInsertElement(){
    const audio = document.getElementById('hibikiplayer');
    audio.addEventListener('loadeddata', this.setCanPlay.bind(this));
    audio.addEventListener('ended', this.finishedTrack.bind(this));
    audio.addEventListener('timeupdate', this.updateCurrentTime.bind(this));
    audio.addEventListener('volumechange', this.updateVolume.bind(this));
    this.set("volume", audio.volume);
  },
  updateCurrentTime(){
    this.set("currentTime", document.getElementById('hibikiplayer').currentTime);
  },
  updateVolume(){
    this.set("volume", document.getElementById('hibikiplayer').volume);
  },
  currentTimeFormatted: Ember.computed("currentTime", function(){

    const minutes = Math.floor(this.get("currentTime")/60);
    let seconds = Math.floor(this.get("currentTime") - (60*minutes));

    if(seconds < 10){
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  }),
  pauseTrackObserver: Ember.observer('pauseTrack', function(){
    this.send("pause");
  }),
  playMusicObserver: Ember.observer('playMusic', function(){
    if(this.get("lastPlayed.id") === this.get("track.id")){
      const player = document.getElementById('hibikiplayer');
      player.play();
      this.set("track.playing", true);
      this.set("playing", true);
    }
  }),
  durationPlayerStyle: Ember.computed('currentTime', 'track.length', function(){
    const endTime = this.get("track.length");
    const currentTime = this.get("currentTime");

    const width = (currentTime/endTime)*100;
    const style = "width: " + width + "%;";
    return Ember.String.htmlSafe(style);
  }),
  volumeBarStyle: Ember.computed('volume', 'muted', function(){
    let style = "";
    if(this.get("muted")){
      style = "height: 0";
    }else{
      let height = 0;
      if(this.get("volume") > 0.9){
        height = 180;
      }else{
        height = 180*this.get("volume");  
      }
      
      style = "height: " + height + "px;"
    }

    return Ember.String.htmlSafe(style);
  }),
  setCanPlay(){
    this.set("canPlay", true);
    this.send("playTrack");
  },
  finishedTrack(){
    this.send("nextTrack");
  },

  actions: {
    seek(event){
      const position = event.clientX - $(".player__duration").offset().left;
      const seekTime = (position * this.get("track.length"))/500; //500 being the width of the seek bar
      document.getElementById('hibikiplayer').currentTime = seekTime;
    },
    setVolume(event){
      if(this.get("muted")){
        this.set("muted", false);
        document.getElementById('hibikiplayer').muted = false;
      }
      const position = event.clientY - ($(".player__volumeBarContainer").offset().top - $(window).scrollTop());
      const volume = (180 - position)/180;
      
      document.getElementById('hibikiplayer').volume = volume;
    },
    toggleMute(){
      if(this.get("muted")){
        this.set("muted", false);
        document.getElementById('hibikiplayer').muted = false;
      }else{
        this.set("muted", true);
        document.getElementById('hibikiplayer').muted = true;
      }
    },
    pause(){
      this.set("playing", false);
      this.set("loading", false);
      this.set("track.playing", false);
      this.set("track.loading", false);
      const player = document.getElementById('hibikiplayer');
      player.pause();
    },
    playTrack(){
      if(this.get("lastPlayed.id") != this.get("track.id")){
        this.set("lastPlayed.playing", false);
        this.set("lastPlayed.loading", false);
      }
      const player = document.getElementById('hibikiplayer');
      player.play();
      this.set("track.playing", true);
      this.set("track.loading", false);
      this.set("playing", true);
      this.set("lastPlayed", this.get("track"))
    },
    nextTrack(){
      const player = document.getElementById('hibikiplayer');
      player.pause();
      this.incrementProperty("position");

      if(this.get("trackList.length") > this.get("position")){
        this.set("track.playing", false);
        this.set("track", null);
        this.set("track", this.get("trackList").objectAt(this.get("position")));
        this.set("track.loading", true);
      }else{
        this.decrementProperty("position");
        this.send("pause");
      }
    },
    previousTrack(){
      const player = document.getElementById('hibikiplayer');
      player.pause();
      this.decrementProperty("position");

      if(this.get("position") >= 0){
        this.set("track.playing", false);
        this.set("track", this.get("trackList").objectAt(this.get("position")));
        this.set("track.loading", true);
      }else{
        this.incrementProperty("position");
        this.send("pause");
      }
    }
  }
});
