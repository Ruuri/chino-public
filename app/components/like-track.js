import Ember from 'ember';
import config from '../config/environment';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  currentUser: Ember.inject.service("current-user"),
  session: Ember.inject.service("session"),

  actions: {
    likeTrack(){
      const endpoint = config.host + "/tracks/" + this.get("track.id") + "/like";
      let store = this.get("store");
      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            // stuff
          })
        }).done(function(response){
          store.pushPayload("track", response);
        });
      });
    },
    unlikeTrack(){
      const endpoint = config.host + "/tracks/" + this.get("track.id") + "/unlike";
      let store = this.get("store");
      this.get('session').authorize('authorizer:devise', (headerName, headerValue) => {
        Ember.$.ajax({
          url: endpoint,
          beforeSend: function(xhr) {
            xhr.setRequestHeader(headerName, headerValue);
          },
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          data: JSON.stringify({
            // stuff
          })
        }).done(function(response){
          store.pushPayload("track", response);
        });
      });
    }
  }
});
