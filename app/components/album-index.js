import Ember from 'ember';

export default Ember.Component.extend({
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",

  actions: {
    playTrack(track, position){
      this.sendAction("playTrack", track, position, this.get("album.orderedTracks"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack")
    }
  }

});
