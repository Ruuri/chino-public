import Ember from 'ember';

const { service } = Ember.inject;

export default Ember.Component.extend({
  tagName: "nav",
  classNames: ["header__nav"],

  session: Ember.inject.service('session'),
  currentUser: service('current-user'),
});
