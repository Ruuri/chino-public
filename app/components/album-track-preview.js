import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ["trackPreview__long"],
  classNameBindings: ["track.playing:playing"],
  playTrack: "playTrack",
  pauseTrack: "pauseTrack",

  actions:{
    playTrack(){
      this.sendAction("playTrack", this.get("track"));
    },
    pauseTrack(){
      this.sendAction("pauseTrack");
    }
  }  
});
