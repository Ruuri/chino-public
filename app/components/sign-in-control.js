import Ember from 'ember';

const { service } = Ember.inject;

export default Ember.Component.extend({
  classNames: ["signIn__container", "actionable"],
  email: "",
  password: "",
  store: Ember.inject.service(),
  needsPassword: true,
  needsEmail: true,
  signingIn: false,
  waiting: false,
  session: service('session'),
  currentUser: service('current-user'),

  validateInput(){
    if(this.get("email.length") === 0){
      this.set("needsEmail", true);
    }else{
      this.set("needsEmail", false);
    }
    if(this.get("password.length") === 0){
      this.set("needsPassword", true);
    }else{
      this.set("needsPassword", false);
    }

    return !(this.get("needsEmail") && this.get("needsPassword"));
  },
  canSignIn: Ember.computed('needsEmail', 'needsPassword', function(){
    return !( this.get("needsPassword") && this.get("needsPassword") );
  }),
  actions: {
    authenticate(){
      if(this.validateInput()){
        let { email, password } = this.getProperties('email', 'password');
        let currentUser = this.get("currentUser");

        this.set("waiting", true);
        return this.get('session').authenticate('authenticator:devise', email, password);
      }
    },
    validate(){
      this.validateInput();
    },
    toggleSignIn(){
      this.toggleProperty("signingIn");
      Ember.run.next(function(){
        $("input.signIn__email").focus();
      })
    }
  }
});
