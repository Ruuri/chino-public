import DS from 'ember-data';

export default DS.Model.extend({
  album: DS.belongsTo('album'),

  artists: DS.hasMany('artist', {async: true}),

  source: DS.attr('string'),
  originalSource: DS.attr('string'),
  title: DS.attr('string'),
  rawLyrics: DS.attr('string'),

  playing: DS.attr('boolean'),
  loading: DS.attr('boolean'),
  liked: DS.attr('boolean'),

  length: DS.attr('number'),
  order: DS.attr('number'),

  time: Ember.computed('length', function(){
    const minutes = Math.floor(this.get("length")/60);
    let seconds = Math.floor(this.get("length") - (minutes*60));

    if(seconds < 10){
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  })
});
