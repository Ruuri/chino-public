import DS from 'ember-data';

export default DS.Model.extend({
  artist: DS.belongsTo('artist'),
  track: DS.belongsTo('track'),

  role: DS.attr('string')
});
