import DS from 'ember-data';

export default DS.Model.extend({
  tracks: DS.hasMany("track", {async: true}),
  artists: DS.hasMany("artist", {async: true}),

  title: DS.attr('string'),
  albumArt: DS.attr('string'),
  albumArtThumb: DS.attr('string'),
  year: DS.attr('string'),

  orderedTracks: Ember.computed.sort("tracks", "trackSort"),
  trackSort: ["order:asc"]
});
  