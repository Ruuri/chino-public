import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  trackholder: DS.attr('string'),   //we use this mainly to save a trip in the case where we want to create a playlist and immediately add a track to it

  owned: DS.attr('boolean'),

  users: DS.hasMany('user', {async: true}),
  tracks: DS.hasMany('track', {async: true})
});
