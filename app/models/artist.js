import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),

  tracks: DS.hasMany('track', {async: true}),
  albums: DS.hasMany('album', {async: true})
});
