import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
const { service } = Ember.inject;

export default Ember.Route.extend(ApplicationRouteMixin, {
  currentUser: service("current-user"),

  beforeModel() {
    return this._loadCurrentUser();
  },

  sessionAuthenticated() {
    this._super(...arguments);
    this._loadCurrentUser();
  },

  _loadCurrentUser() {
    return this.get('currentUser').load().catch(() => this.get('session').invalidate());
  },
  
  actions: {
    didTransition(){
      window.scrollTo(0,0);
    },
    playTrack(track, position, trackList){
      this.controller.set('selectedTrack', track);
      this.controller.set('trackList', trackList);
      this.controller.set('position', position);
      this.controller.toggleProperty('playMusic');
    },
    pauseTrack() {
      this.controller.toggleProperty('pauseTrack');
    }
  }
});
