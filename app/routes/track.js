import Ember from 'ember';

export default Ember.Route.extend({
  serialize(track, params){
    return {
      track_id: track.get("id"),
      title: track.get("title")
    }
  },
  afterModel(model){
    document.title = this.modelFor('track').get('title') + " | animemusic";
  }
});
