import Ember from 'ember';

export default Ember.Controller.extend({
  playMusic: false,
  selectedTrack: null,
  trackList: null,
  position: 0,
  pauseTrack: false,
  windowWidth: null,
  windowHeight: null,

  init(){
    this.setWindows();
    window.addEventListener('resize', this.setWindows.bind(this));
  },
  setWindows(){
    this.set("windowHeight", window.innerHeight || document.body.clientHeight);
    this.set("windowWidth", window.innerWidth || document.body.clientWidth);
  },
  bodyStyle: Ember.computed("windowWidth", function(){
    const contentWidth = this.get("windowWidth") - 267;

    const style = "width: " + contentWidth + "px;";
    const width = contentWidth + "px;";
    $(".header-poop").css("width", width);
    return Ember.String.htmlSafe(style);
  })
});
