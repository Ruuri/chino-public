import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('last-listened-to-index', 'Integration | Component | last listened to index', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{last-listened-to-index}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#last-listened-to-index}}
      template block text
    {{/last-listened-to-index}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
