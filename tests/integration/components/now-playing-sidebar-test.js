import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('now-playing-sidebar', 'Integration | Component | now playing sidebar', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{now-playing-sidebar}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#now-playing-sidebar}}
      template block text
    {{/now-playing-sidebar}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
