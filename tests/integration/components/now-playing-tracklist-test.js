import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('now-playing-tracklist', 'Integration | Component | now playing tracklist', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{now-playing-tracklist}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#now-playing-tracklist}}
      template block text
    {{/now-playing-tracklist}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
