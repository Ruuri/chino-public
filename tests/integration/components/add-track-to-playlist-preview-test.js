import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('add-track-to-playlist-preview', 'Integration | Component | add track to playlist preview', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{add-track-to-playlist-preview}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#add-track-to-playlist-preview}}
      template block text
    {{/add-track-to-playlist-preview}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
