import Ember from 'ember';
import TrackActionsMixin from 'chino/mixins/track-actions';
import { module, test } from 'qunit';

module('Unit | Mixin | track actions');

// Replace this with your real tests.
test('it works', function(assert) {
  let TrackActionsObject = Ember.Object.extend(TrackActionsMixin);
  let subject = TrackActionsObject.create();
  assert.ok(subject);
});
