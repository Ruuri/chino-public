import Ember from 'ember';
import TrackUserActionsMixin from 'chino/mixins/track-user-actions';
import { module, test } from 'qunit';

module('Unit | Mixin | track user actions');

// Replace this with your real tests.
test('it works', function(assert) {
  let TrackUserActionsObject = Ember.Object.extend(TrackUserActionsMixin);
  let subject = TrackUserActionsObject.create();
  assert.ok(subject);
});
